import React, { Component } from "react";

import "./App.css";
import Homepage from "./common/Homepage"
import Login from "./Auth/Login";
import { BrowserRouter as Router, Route } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/" component={Homepage} />
        <Route exact path="/login" component={Login} />
      </Router>
    );
  }
}

export default App;
