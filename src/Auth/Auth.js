
class Auth {
    constructor(){
        this.authenticated=false;
        this.user= {
                    idUser: "",
                    username: "",
                    password: "",
                    user_type: "",
                    email: ""
                }
        }
    setUser(u){
        this.user=u;
    }
    getUser(){
        return this.user;
    }

    login(callback){
        this.authenticated=true;
        callback();
    }
    logout(callback){
        this.authenticated=false;
        callback();
    }
    isAuthenticated(){
        return this.authenticated;
    }
  
}
export default new Auth;