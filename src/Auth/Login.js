import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Login.css";
import axios from "axios";
import Auth from "./Auth";

const emailRegex = RegExp(
  ///^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@?[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

class Login extends Component {
    emptyUser = {
        idUser: "",
        username: "",
        password: "",
        user_type: "",
        email: ""
    };

    constructor(props) {
        super(props);
        this.state = {
          email: null,
          password: null,
          formErrors: {
            email: "",
            password: ""
          },
          user: this.emptyUser
        };
        this.handleSubmit = this.handleSubmit.bind(this);
      }

      async handleSubmit(e) {
        e.preventDefault();
        console.log(`
            --SUBMITTING--
            Email: ${this.state.email}
            Password: ${this.state.password}
          `);
        let res = await axios.get(
          "users/user/" + this.state.email + "-" + this.state.password
        );
        this.setState({ user: res.data });
        if (
          this.state.user.email === this.state.email &&
          this.state.user.password === this.state.password
        ) {
          Auth.setUser(this.state.user);
          Auth.login(() => {
            this.props.history.push("/");
          });
        } else {
        }
      }

      handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        let formErrors = { ...this.state.formErrors };
        switch (name) {
          case "email":
            formErrors.email = emailRegex.test(value)
              ? ""
              : "invalid email address";
            break;
          case "password":
            formErrors.password =
              value.length < 4 ? "minimum 4 characaters required" : "";
            break;
          default:
            break;
        }
        this.setState({ formErrors, [name]: value }, () => console.log(this.state));
      };
    
      render() {
        const { formErrors } = this.state;
    
        return (
          <div className="wrapper">
            <div className="form-wrapper">
              <h1>Log In</h1>
              <form onSubmit={this.handleSubmit} noValidate>
                <div className="email">
                  <label htmlFor="email">Email</label>
                  <input
                    className={formErrors.email.length > 0 ? "error" : null}
                    placeholder="Email"
                    type="email"
                    name="email"
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.email.length > 0 && (
                    <span className="errorMessage">{formErrors.email}</span>
                  )}
                </div>
                <div className="password">
                  <label htmlFor="password">Password</label>
                  <input
                    className={formErrors.password.length > 0 ? "error" : null}
                    placeholder="Password"
                    type="password"
                    name="password"
                    noValidate
                    onChange={this.handleChange}
                  />
                  {formErrors.password.length > 0 && (
                    <span className="errorMessage">{formErrors.password}</span>
                  )}
                </div>
                <div className="blueButton">
                  <button type="submit">Log in</button>
                  <Link to="/register">
                    <button type="submit">Register</button>
                  </Link>
                </div>
              </form>
            </div>
          </div>
        );
      }
    }
    
    export default Login;
    