import React from 'react';  
import { Table,Button } from 'react-bootstrap';  
import axios from 'axios';  
  
const apiUrl = 'http://localhost:8080/pstema1';  
  
class UserList extends React.Component{  
    constructor(props){  
        super(props);  
        this.state = {  
           error:null,  
           users:[],  
           response: {}  
              
        }  
    }  
  
    componentDidMount(){  
       axios.get(apiUrl + '/getAll').then(response => response.data).then(  
            (result)=>{  
                this.setState({  
                    users:result  
                });  
            },  
            (error)=>{  
                this.setState({error});  
            }  
        )  
    }  

    deleteUser(userId) {  
        const { users } = this.state;     
       axios.delete(apiUrl + '/delete/' + userId).then(result=>{  
         alert(result.data);  
          this.setState({  
            response:result,  
            users:users.filter(user=>user.UserId !== userId)  
          });  
        });  
      }  

      render(){         
        const{error,users}=this.state;  
        if(error){  
            return(  
                <div>Error:{error.message}</div>  
            )  
        }  
        else  
        {  
            return(  
         <div>  
                      
                  <Table>  
                    <thead className="btn-primary">  
                      <tr>  
                        <th>Username</th>  
                        <th>EmailId</th>  
                        <th>Password</th>  
                        <th>UserType</th>  
                        <th>Action</th>  
                      </tr>  
                    </thead>  
                    <tbody>  
                      {users.map(user => (  
                        <tr key={user.UserId}>  
                          <td>{user.Username}</td>  
                          <td>{user.EmailID}</td>  
                          <td>{user.Password}</td>  
                          <td>{user.UserType}</td>  
                          <td><Button variant="info" onClick={() => this.props.editUser(user.UserId)}>Edit</Button>       
                          <Button variant="danger" onClick={() => this.deleteUser(user.UserId)}>Delete</Button>  
                          
                          </td>  
                        </tr>  
                      ))}  
                    </tbody>  
                  </Table>  
                </div>  
              )  
        }  
    }  
}  

export default UserList; 