import React, { Component } from "react";
import "../App.css";
import NavigationBar from "../navbar/NavigationBar";
import Background from './roastery.jpg'

var sectionStyle = {
    width: "100%",
    height: "2000px",
    backgroundImage: `url(${Background})`
  };

export default class Homepage extends Component {
    
    render() {
      return (
         
        <section style={ sectionStyle }>
            <NavigationBar />
        </section>
      );
    }
  }