import React from "react";
import { Link, NavLink } from "react-router-dom";
const NavigationBar = () => {
  return (
    <nav className="nav-wrapper grey darken-3">
      <div className="container">
        <ul className="left">
          <li>
            <NavLink to="/" className="left brand-logo">
              {" "}
              Coffee Roastery{" "}
            </NavLink>
          </li>
        </ul>

        <ul className="right">
          <li>Open For Business</li>
          <li>
            <NavLink to="/pstema1">USERS</NavLink>
          </li>
          <li>
            <NavLink to="/coffee">COFFEE</NavLink>
          </li>
          <NavLink to="/orders">ORDERS</NavLink>
        </ul>
      </div>
    </nav>
  );
};

export default NavigationBar;